#!/usr/bin/env python
from peewee import *
import datetime

db = SqliteDatabase('database.sqlite')

class BaseModel(Model):
    class Meta:
        database = db

class User(BaseModel):
    username = CharField(unique=True)

class Message(BaseModel):
    user = ForeignKeyField(User, backref='messages')
    message = TextField()
    created_date = DateTimeField(default=datetime.datetime.now)