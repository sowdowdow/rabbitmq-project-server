### Authors :
Simon DORMEAU  
Maxime FOUQUES  
Lucille TYRAKOWSKI  

-----------
This project aim to receive some user and messages via RabbitMQ,
and then store it into a SQLite database.


In order to use it, you need to follow these steps : 

1. create a virtualenv and activate it via :  
   `virtualenv <afolder>` and `source bin/activate`
2. place the project folder inside this folder
3. install python requirements :  
   `pip install -r Project/requirements.txt`

4. go into the project directory (this is very important in order to use the DB) :  
   `cd <Project>`
5. launch the project :  
   `python rabbitMQ-receiver.py`
6. wait for messages :) (see client project)

Finally the project should looks like this :

```
YourVirtualenvFolder  
    |   
    |----ProjectFolder  
            |  
            |--- rabbitMQ-receiver.py
            |--- database.sqlite
```
###### 2019