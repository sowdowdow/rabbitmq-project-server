#!/usr/bin/env python
import json
import pika
from messageCreator import createMessage

connection = pika.BlockingConnection(pika.ConnectionParameters("localhost"))

channel = connection.channel()
channel.queue_declare(queue="message")


def callback(ch, method, properties, body):
    print("Received : ", body)
    res = json.loads(body.decode('utf8'))
    createMessage(res['user'], res['message'])


channel.basic_consume(callback, queue="message", no_ack=True)

print('Wainting for messages...')
channel.start_consuming()
