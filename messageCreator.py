#!/usr/bin/env python
from peewee import *
from models import *

'''
This function allow you to add a message to the database
if the user doesn't already exists, it is created
'''
def createMessage(name, message):
    try:
        user = User.get(User.username == name)
        print("user found : ", name)
    except:
        # if no user is found, we create it
        user = User(username=name)
        user.save()
        print("user created : ", name)

    message = Message(user=user, message=message)
    message.save()