#!/usr/bin/env python
from peewee import *
from models import Message, User

db = SqliteDatabase('database.sqlite')

db.connect()
db.create_tables([User, Message])