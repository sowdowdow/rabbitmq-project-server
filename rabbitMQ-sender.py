#!/usr/bin/env python
import pika

# This file is for test purpose only, don't use it ! ! !

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))

channel = connection.channel()
channel.queue_declare(queue="messae")

channel.basic_publish(
    exchange='',
    routing_key='message',
    body='{"user":"gerardo","message":"a test message"}'
    )

connection.close()